<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'qwerty');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Bl5I,xHS XY|^Trlw(a`D/sLll<`|_Gk li)J]w:clD?`0LaQ4U-Ffs{%mZ_o_}9');
define('SECURE_AUTH_KEY',  ',|O/lJJ*}/LX1+$g LE5_-dIz} #<0SML;.8(tzt?Lt7OV@z^-Q;LO{h}]eY9drB');
define('LOGGED_IN_KEY',    'vU!dxlO -A-ovydH,odJ[#H$l,?IDeo}o@[|4Cj[aJ>u$5TB_ sZT~5EQ[9 fy1B');
define('NONCE_KEY',        '?wl&F@Y5!S-{wM5 hi:L,k(/ZdkV}7}V0-&8ybbm(stu6sV*Z_f1l4V>E%{6 l;)');
define('AUTH_SALT',        '~EnAp^JP$u=iu~k//iq5qi[rM3Mg/Z`:?.w0=m8DMk|_`|fl@M}2g;V*DwiV].L+');
define('SECURE_AUTH_SALT', '0UU&Z38bqxs3@%eAD/BWo(5[BBND|%uK;7gDvS$ovG_z kV1P.lw&J,3PCD[=IJN');
define('LOGGED_IN_SALT',   '}^T5`]92:lVOV+6@9~{kf~n|~;ApBOX5uq$s_q3*o4XDWX<h><wTb D<Ud{*>|*`');
define('NONCE_SALT',       'tWMRu%>uk`=9KJ[W # q6pwHbvk8C*@+dP9n)YnG|30X4yO}[::@3IuVNZZ^VwT|');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

